const express = require('express');

const app = express();
const PORT = 3000;

app.use(express.json());

app.listen(PORT, () => console.log(`Express server currently running on port ${PORT}`));

app.get('/', (request, response) => {
  response.send('Hello Welcome to the Landing Page');
});

app.get('/profile', (request, response) => {
  response.send('This will be the profile Section');
});

app.get('/login', (request, response) => {
  response.send('This will be the Login Section');
});

app.get('/register', (request, response) => {
  response.send('This will be the Register Section');
});